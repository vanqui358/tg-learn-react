import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import TodolistComponent from './ToDo/TodolistComponent.js'


const  Routes  = () =>(
   <Router>
      <Scene key = "root">
         <Scene key = "home" component = {TodolistComponent} title = "Home" initial = {true} />
         
      </Scene>
   </Router>
)
export default Routes