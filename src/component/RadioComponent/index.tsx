import * as React from 'react';
import {View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {useAppDispatch} from '../../redux/app/hook';
import {checkStep} from '../../redux/features/taskSlice';
interface ParentCompProps {
  childRadio: {
    id: number;
    name: string;
    nameTag: string;
    step: boolean;
  };
}

const RadioComponent: React.FC<ParentCompProps> = props => {
  const dispatch = useAppDispatch();
  const [checked, setChecked] = React.useState(false);

  const handelRadio = () => {
    setChecked(e => !e);
    const checkData = {
      id: props.childRadio.id,
      name: props.childRadio.name,
      nameTag: props.childRadio.nameTag,
      step: checked,
    };
    dispatch(checkStep(checkData));
  };

  return (
    <View>
      <RadioButton
        color={'black'}
        value="first"
        status={checked === true ? 'checked' : 'unchecked'}
        onPress={handelRadio}
      />
    </View>
  );
};

export default RadioComponent;
