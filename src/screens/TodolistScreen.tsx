import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import {useAppSelector} from '../redux/app/hook';
import {ToDoStackNavigator} from '../navigation/index';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';
import ToDoComponent from '../component/ToDoComponent';
import moment from 'moment';

type addTaskScreenNavigation = NativeStackNavigationProp<
  ToDoStackNavigator,
  'AddTask'
>;

const TodolistScreen = () => {
  const navigation = useNavigation<addTaskScreenNavigation>();
  const listTasks = useAppSelector(state => state.tasks);

  const currentDate = moment().format('LL');

  return (
    <View style={styles.wrapper}>
      <View style={styles.listTask}>
        <ToDoComponent />
      </View>
      <Text style={styles.date}>{currentDate}</Text>
      <Text style={styles.numberTask}> {listTasks.length} Task</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate('AddTask')}
        style={styles.addTask}>
        <View style={styles.addTaskElement}>
          <Text style={styles.add}>＋</Text>
        </View>
        <Text style={styles.addTaskEllipse}></Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    width: 400,
    height: '100%',
    backgroundColor: '#FFF500',
  },
  addTask: {
    position: 'absolute',
    width: 56,
    height: 56,
    left: 310,
    top: 570,
  },

  addTaskElement: {
    position: 'absolute',
    left: 28.57,
    right: 28.57,
    top: 28.57,
    bottom: 28.57,
  },
  add: {
    position: 'absolute',
    zIndex: 1,
    color: '#fff',
    borderRadius: 4,
    fontSize: 35,
    top: -25,
    left: -17.5,
  },
  addTaskEllipse: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    borderRadius: 50,
    backgroundColor: '#303030',
  },
  listTask: {
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    gap: 24,
    position: 'absolute',
    width: 289,
    height: 326,
    left: 24,
    top: 180.45,
  },
  date: {
    position: 'absolute',
    width: 251,
    height: 38,
    left: 24,
    top: 73.45,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 32,
    lineHeight: 38,
    color: '#303030',
  },
  numberTask: {
    position: 'absolute',
    width: 78,
    height: 29,
    left: 24,
    top: 119.45,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 24,
    lineHeight: 29,
    /* identical to box height */
    color: '#303030',
  },
});

export default TodolistScreen;
