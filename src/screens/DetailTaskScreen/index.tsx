import React, { useState } from 'react';
import {
  TouchableOpacity,
  TextInput,
  View,
  Text,
  Alert,
  StyleSheet,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCalendarDay, faXmark} from '@fortawesome/free-solid-svg-icons';
import {RouteProp, useRoute, useNavigation} from '@react-navigation/native';
import {useAppDispatch,useAppSelector} from '../../redux/app/hook';
import {deleTaskToDo,} from '../../redux/features/taskSlice';
import {ToDoStackNavigator} from '../../navigation/index';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import Check from '../../assets/svg/iconCheck.svg';

import {Tags} from './components';
type detailScreenRoute = RouteProp<ToDoStackNavigator, 'DetailTask'>;
type homeScreen = NativeStackNavigationProp<ToDoStackNavigator, 'Home'>;

const  DetailTaskScreen = () => {
  // get id || name 
  const {
    params: {id, name, nameTag},
  } = useRoute<detailScreenRoute>();

  const navigation = useNavigation<homeScreen>();
  const listTasks = useAppSelector(state => state.tasks);

  console.log(listTasks);

  const dispatch = useAppDispatch();
  
  //display alert Delete Task
  const displayDeleteTaskAlert = () => {
    Alert.alert(
      'Are You Sure!',
      'This action will delete your task',
      [
        {
          text: 'Delete',
          onPress: deleteTask,
        },
        {
          text: 'No Thank',
          onPress: () => console.log('no thank'),
        },
      ],
      {
        cancelable: true,
      },
    );
  };

  //Delete Task
  const deleteTask = () => {
    dispatch(deleTaskToDo(id));
    Alert.alert('Delete Success');
    navigation.navigate('Home');
  };

  const checkDone = () => {
    Alert.alert('Add Success');
    navigation.navigate('Home');
  };

  return (
    <View style={styles.addTaskList}>
      <View style={styles.mainAddTask}>
        <View style={styles.toDo}>
          <Text style={styles.titleToDo}>To-Do</Text>
          <Text></Text>
          <TextInput
            style={styles.nameToDo}
            value={name}
            placeholder="Insert your text!"
          />
        </View>
        <Text style={styles.titleTagDetail}>Tags</Text>
          {listTasks.map((name,index)=>(
           <View style={styles.listTag}>
            {id==index?(<Text key={index} style={styles.nameAddTag}>{name.tag}</Text>):(null)}
            </View>
          ))}
    
        <Tags 
        idTask={id} 
        name={name}
        nameTag={nameTag}
        />
        <View style={styles.deadline}>
          <Text style={styles.titleDeadline}>Deadline</Text>
          <View style={styles.deadlineText}>
            <FontAwesomeIcon style={{color: '#fff'}} icon={faCalendarDay} />
            <Text style={styles.nameDeadLine}>Today</Text>
            <Text style={styles.iconDeadline}></Text>
          </View>
        </View>
      </View>
      <View style={styles.button}>
        <TouchableOpacity
          onPress={displayDeleteTaskAlert}
          style={styles.buttonRemove}>
          <FontAwesomeIcon
            style={{color: '#FFFFFF'}}
            size={25}
            icon={faXmark}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={checkDone} style={styles.buttonCheck}>
          <Check width={25} height={25} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  addTaskList: {
    position: 'relative',
    width: 400,
    height: 844,
    backgroundColor: '#303030',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: 0,
    position: 'absolute',
    width: 340,
    height: 56,
    left: 25,
    top: 550,
    justifyContent: 'space-between',
  },
  buttonRemove: {
    backgroundColor: '#BF1616',
    padding: 15,
    borderRadius: 50,
  },
  buttonCheck: {
    backgroundColor: '#fff',
    padding: 15,
    borderRadius: 50,
  },
  mainAddTask: {
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    gap: 31,
    position: 'absolute',
    width: 294,
    height: 227,
    left: 24,
    top: 153.45,
  },
  toDo: {
    width: 294,
    height: 58,
    top:15,
  },
  titleToDo: {
    position: 'absolute',
    width: 48,
    height: 19,
    left: 0,
    top: 0,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  nameToDo: {
    position: 'absolute',
    width: 293,
    height: 50,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#FFFFFF',
    borderColor: '#303030',
    borderWidth: 1,
  },
  tag: {
    width: 104,
    height: 51,
    /* Inside auto layout */
  },
  titleTag: {
    position: 'absolute',
    width: 38,
    height: 19,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },

  titleTagDetail: {
    position: 'absolute',
    width: 38,
    height: 19,
    left: 0,
    top: 75,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  addTags: {
    position: 'absolute',
    width: 104,
    height: 24,
    left: 0,
    top: 110,
  },
  nameAddTags: {
    position: 'absolute',
    width: 72,
    height: 19,
    left: 32,
    top: -65,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFFFFF',
  },
  iconAdd: {
    position: 'absolute',
    width: 24,
    height: 24,
    left: 5,
    top: -64,
    zIndex: 1,
  },
  ellipseIconAdd: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: -65,
    bottom: 65,
    paddingLeft: 7,
    paddingTop: 5,
    backgroundColor: '#ffff',
    borderRadius: 50,
    width: 25,
  },


  addTagsDetail: {
    position: 'absolute',
    width: 104,
    height: 24,
    left: 0,
    top: 125,
  },

  iconEdit: {
    position: 'absolute',
    width: 24,
    height: 24,
    left: 5,
    top: -60.5,
    zIndex: 1,
  },

  deadline: {
    width: 96,
    height: 56,
  },
  titleDeadline: {
    position: 'absolute',
    width: 71,
    height: 19,
    left: 0,
    top: 40,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  nameAddTag: {
    left: 0,
    top: 0,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
  },

  deadlineText: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    gap: 5,
    position: 'absolute',
    width: 96,
    height: 29,
    left: 0,
    top: 70,
  },
  nameDeadLine: {
    width: 67,
    height: 29,
    marginLeft: 10,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#fff',
    /* Inside auto layout */
  },
  iconDeadline: {
    width: 24,
    height: 24,
  },
  tagTitle: {
    width: 200,
    height: 45,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 24,
    color: '#303030',
    padding: 10,
    /* Inside auto layout */
  },
  listTag: {
    display: 'flex',
    top: 55,
    width: 340,
    height: 19,
    flexWrap: 'wrap',
    /* Inside auto layout */
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
  },

  tagGroupAdd: {
    height: 50,
    width: 250,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fff',
    color: '#C5C5C5',
    padding: 10,
    fontSize: 20,
  },
  tagGroupButtonAdd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    gap: 10,
    height: 39,
    /* Inside auto layout */
    order: 2,
    alignSelf: 'stretch',
  },
  addText: {
    width: 32,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
  },
});

export default DetailTaskScreen;
