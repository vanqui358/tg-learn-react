import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import type {RootState} from '../app/store';

interface TaskState {
  id: number;
  text: string;
  tag: string;
  step: boolean;
}
const initialState: TaskState[] = [];
export const taskSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTaskToDo: (
      state,
      action: PayloadAction<{name: string; nameTag: string}>,
    ) => {
      state.push({
        id: state.length,
        text: action.payload.name,
        tag: action.payload.nameTag,
        step: false,
      });
    },
    deleTaskToDo: (state, action: PayloadAction<number>) => {
      state.splice(action.payload, 1);
    },
    addTag: (
      state,
      action: PayloadAction<{id: number; name: string; nameTag: string}>,
    ) => {
      state.splice(action.payload.id, 1, {
        id: state.length,
        text: action.payload.name,
        tag: action.payload.nameTag,
        step: false,
      });
    },
    deleteTag: (state, action: PayloadAction<{id: number; name: string}>) => {
      state.splice(action.payload.id, 1, {
        id: state.length,
        text: action.payload.name,
        tag: '',
        step: false,
      });
    },
    editTag: (
      state,
      action: PayloadAction<{id: number; name: string; nameTag: string}>,
    ) => {
      state.splice(action.payload.id, 1, {
        id: state.length,
        text: action.payload.name,
        tag: action.payload.nameTag,
        step: false,
      });
    },
    checkStep: (
      state,
      action: PayloadAction<{
        id: number;
        name: string;
        nameTag: string;
        step: boolean;
      }>,
    ) => {
      state.splice(action.payload.id, 1, {
        id: state.length,
        text: action.payload.name,
        tag: action.payload.nameTag,
        step: action.payload.step,
      });
    },
  },
});

export const {
  addTaskToDo,
  deleTaskToDo,
  addTag,
  deleteTag,
  editTag,
  checkStep,
} = taskSlice.actions;
export const listTasks = (state: RootState) => state.tasks;
export default taskSlice.reducer;
