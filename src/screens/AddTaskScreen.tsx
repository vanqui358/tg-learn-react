import React from 'react';
import {useState} from 'react';
import {
  TouchableOpacity,
  TextInput,
  View,
  Text,
  Alert,
  StyleSheet,
  Modal,
} from 'react-native';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCalendarDay} from '@fortawesome/free-solid-svg-icons';
import {useAppDispatch} from '../redux/app/hook';
import {addTaskToDo} from '../redux/features/taskSlice';

const AddTaskScreen = () => {
  const [task, setTask] = useState('');
  const dispatch = useAppDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const [tag, setTag] = useState('');
  // get list Task from AysncStorage

  /// Add Task
  const handelTask = () => {
    if (task === '') {
      Alert.alert('Hãy nhập tên task của bạn');
      // refTask.current.focus();
    } else {
      Alert.alert('Add success');
      const addToDo = {name: task, nameTag:"#"+ tag};
      dispatch(addTaskToDo(addToDo));
      // refTask.current.focus();
    }
  };

  const handelListTag = () => {
    if (tag == '') {
      Alert.alert('Cần phải nhập');
    } else {
      setModalVisible(false);
      Alert.alert('Add Tag Success');

    }
  };

  return (
    <View style={styles.addTaskList}>
      <View style={styles.mainAddTask}>
        <View style={styles.toDo}>
          <Text style={styles.titleToDo}>To-Do</Text>
          <Text></Text>
          <TextInput
            style={styles.nameToDo}
            onChangeText={text => setTask(text)}
            value={task}
            placeholder="Insert your text!"
            placeholderTextColor="#FFFFFF"
          />
        </View>
        <View style={styles.tag}>
          <Text style={styles.titleTag}>Tags</Text>
          <TouchableOpacity
            style={styles.addTags}
            onPress={() => setModalVisible(e => !e)}>
            <Text style={styles.nameAddTags}>Add Tags</Text>

            <Text style={styles.iconAdd}>＋</Text>
            <Text style={styles.ellipseIconAdd}></Text>
          </TouchableOpacity>

          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
              setModalVisible(!modalVisible);
            }}>
            <View style={styles.modalBackground}>
              <View style={styles.modalContainer}>
                <Text style={styles.tagTitle}>Add Tags</Text>
                <TextInput
                  style={styles.tagGroupAdd}
                  placeholder="Add tags"
                  value={tag}
                  onChangeText={text => setTag(text)}
                />
                <TouchableOpacity
                  onPress={handelListTag}
                  style={styles.tagGroupButtonAdd}>
                  <Text style={styles.addText}>Add</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>

        <View style={styles.deadline}>
          <Text style={styles.titleDeadline}>Deadline</Text>
          <View style={styles.deadlineText}>
            <FontAwesomeIcon style={{color: '#fff'}} icon={faCalendarDay} />
            <Text style={styles.nameDeadLine}>Today</Text>
            <Text style={styles.iconDeadline}></Text>
          </View>
        </View>
      </View>
      <TouchableOpacity onPress={() => handelTask()} style={styles.save}>
        <Text style={{color: '#FFF', fontWeight: '300'}}>Save</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  addTaskList: {
    position: 'relative',
    width: 400,
    height: 844,
    backgroundColor: '#303030',
  },
  save: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 10,
    position: 'absolute',
    width: 340,
    height: 48,
    left: 25,
    top: 600,
    backgroundColor: '#F7CF00',
    borderRadius: 4,
  },
  mainAddTask: {
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    gap: 31,
    position: 'absolute',
    width: 294,
    height: 227,
    left: 24,
    top: 153.45,
  },
  toDo: {
    width: 294,
    height: 58,
  },
  titleToDo: {
    position: 'absolute',
    width: 48,
    height: 19,
    left: 0,
    top: 0,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  nameToDo: {
    position: 'absolute',
    width: 293,
    height: 50,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#FFFFFF',
    borderColor: '#303030',
    borderWidth: 1,
  },
  tag: {
    width: 104,
    height: 51,
    /* Inside auto layout */
  },
  titleTag: {
    position: 'absolute',
    width: 38,
    height: 19,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  deadline: {
    width: 96,
    height: 56,
  },
  addTags: {
    position: 'absolute',
    width: 104,
    height: 24,
    left: 0,
    top: 110,
  },
  nameAddTags: {
    position: 'absolute',
    width: 72,
    height: 19,
    left: 32,
    top: -65,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFFFFF',
  },
  iconAdd: {
    position: 'absolute',
    width: 24,
    height: 24,
    left: 5,
    top: -64,
    zIndex: 1,
  },
  ellipseIconAdd: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: -65,
    bottom: 65,
    backgroundColor: '#ffff',
    borderRadius: 50,
    width: 25,
  },
  titleDeadline: {
    position: 'absolute',
    width: 71,
    height: 19,
    left: 0,
    top: 40,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  deadlineText: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    gap: 5,
    position: 'absolute',
    width: 96,
    height: 29,
    left: 0,
    top: 70,
  },
  nameDeadLine: {
    width: 67,
    height: 29,
    marginLeft: 10,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#fff',
    /* Inside auto layout */
  },
  iconDeadline: {
    width: 24,
    height: 24,
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  tagTitle: {
    width: 200,
    height: 45,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 24,
    color: '#303030',
    padding: 10,
    /* Inside auto layout */
  },
  tagGroupAdd: {
    height: 50,
    width: 250,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fff',
    color: '#C5C5C5',
    padding: 10,
    fontSize: 20,
  },
  tagGroupButtonAdd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    gap: 10,
    height: 39,
    /* Inside auto layout */
    order: 2,
    alignSelf: 'stretch',
  },
  addText: {
    width: 32,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
  },
});

export default AddTaskScreen;
