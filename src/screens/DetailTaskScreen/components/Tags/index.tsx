
import React, { useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Edit from '../../../../assets/svg/iconEdit.svg';
import {TagModal} from './TagModal';
interface Props {
  idTask:number;
  name:string;
  nameTag:string;
}
export const Tags = ({
  idTask,
  name,
  nameTag,
}: Props) => {
 console.log(nameTag);
 
  const [modalVisible, setModalVisible] = useState(false);
  const [isShowEdit, setIsShowEdit] = useState('add');

  useEffect(() => {
    if (nameTag.length != 0) {
      setIsShowEdit('edit');
    }
  }, []);
  return (
    <View>
      <TagModal
        idTask={idTask}
        visible={modalVisible}
        onDismiss={() => setModalVisible(false)}
        type={nameTag.length != 0 ? 'edit' : 'add'}
        value={nameTag}
        nameTask={name}
      />
      <View style={styles.tag}>
        {isShowEdit == 'add' ? (
          <TouchableOpacity
            style={styles.addTags}
            onPress={() => setModalVisible(e => !e)}>
            <Text style={styles.nameAddTags}>Add Tags</Text>
            <Text style={styles.iconAdd}>＋</Text>
            <Text style={styles.ellipseIconAdd}></Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.addTagsDetail}
            onPress={() => setModalVisible(e => !e)}>
            <Text style={styles.nameAddTags}>Edit tags</Text>
            <Text style={styles.ellipseIconAdd}>
              <Edit width={15} height={15} />
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  addTaskList: {
    position: 'relative',
    width: 400,
    height: 844,
    backgroundColor: '#303030',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: 0,
    position: 'absolute',
    width: 340,
    height: 56,
    left: 25,
    top: 550,
    justifyContent: 'space-between',
  },
  buttonRemove: {
    backgroundColor: '#BF1616',
    padding: 15,
    borderRadius: 50,
  },
  buttonCheck: {
    backgroundColor: '#fff',
    padding: 15,
    borderRadius: 50,
  },
  mainAddTask: {
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    gap: 31,
    position: 'absolute',
    width: 294,
    height: 227,
    left: 24,
    top: 153.45,
  },
  toDo: {
    width: 294,
    height: 58,
  },
  titleToDo: {
    position: 'absolute',
    width: 48,
    height: 19,
    left: 0,
    top: 0,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  nameToDo: {
    position: 'absolute',
    width: 293,
    height: 50,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#FFFFFF',
    borderColor: '#303030',
    borderWidth: 1,
  },
  tag: {
    width: 104,
    height: 51,
    /* Inside auto layout */
  },
  titleTag: {
    position: 'absolute',
    width: 38,
    height: 19,
    left: 0,
    top: 15,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  listTag: {
    display: 'flex',
    top: 50,
    width: 340,
    height: 19,
    flexWrap: 'wrap',
    /* Inside auto layout */
  },
  titleTagDetail: {
    position: 'absolute',
    width: 38,
    height: 19,
    left: 0,
    top: 20,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  addTags: {
    position: 'absolute',
    width: 104,
    height: 24,
    left: 0,
    top: 110,
  },
  nameAddTags: {
    position: 'absolute',
    width: 72,
    height: 19,
    left: 32,
    top: -65,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFFFFF',
  },
  iconAdd: {
    position: 'absolute',
    width: 24,
    height: 24,
    left: 5,
    top: -64,
    zIndex: 1,
  },
  ellipseIconAdd: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: -65,
    bottom: 65,
    paddingLeft: 7,
    paddingTop: 5,
    backgroundColor: '#ffff',
    borderRadius: 50,
    width: 25,
  },

  nameAddTag: {
    left: 0,
    top: 0,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
  },

  addTagsDetail: {
    position: 'absolute',
    width: 104,
    height: 24,
    left: 0,
    top: 125,
  },

  iconEdit: {
    position: 'absolute',
    width: 24,
    height: 24,
    left: 5,
    top: -60.5,
    zIndex: 1,
  },

  deadline: {
    width: 96,
    height: 56,
  },
  titleDeadline: {
    position: 'absolute',
    width: 71,
    height: 19,
    left: 0,
    top: 40,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    /* identical to box height */
    color: '#FFF500',
  },
  deadlineText: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    gap: 5,
    position: 'absolute',
    width: 96,
    height: 29,
    left: 0,
    top: 70,
  },
  nameDeadLine: {
    width: 67,
    height: 29,
    marginLeft: 10,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 29,
    /* identical to box height */
    color: '#fff',
    /* Inside auto layout */
  },
  iconDeadline: {
    width: 24,
    height: 24,
  },
  tagTitle: {
    width: 200,
    height: 45,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 24,
    color: '#303030',
    padding: 10,
    /* Inside auto layout */
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
  },

  tagGroupAdd: {
    height: 50,
    width: 250,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fff',
    color: '#C5C5C5',
    padding: 10,
    fontSize: 20,
  },
  tagGroupButtonAdd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    gap: 10,
    height: 39,
    /* Inside auto layout */
    order: 2,
    alignSelf: 'stretch',
  },
  addText: {
    width: 32,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
  },

  editTag: {
    /* Auto layout */

    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    gap: 10,
    width: 320,
    height: 39,
    /* Inside auto layout */
  },

  removeTag: {
    width: 63,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#BF1616',
    /* Inside auto layout */
    flexGrow: 0,
  },
  saveEditTag: {
    width: 39,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
    flexGrow: 0,
  },
});
