import React from "react"
import { Provider } from "react-redux"
import store from './src/redux/app/store'
import ToDoAp from './src/navigation/ToDoAp'


const  App = () =>{
  return(
<Provider store={store}>

    <ToDoAp />
  </Provider>
  )
}
export default App