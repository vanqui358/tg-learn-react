import React, {useState} from 'react';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
interface Props {
  idTask: number;
  type: 'add' | 'edit';
  visible: boolean;
  onDismiss: () => void;
  value: string;
  nameTask: string;
}
import {useAppDispatch} from '../../../../redux/app/hook';
import {addTag, deleteTag} from '../../../../redux/features/taskSlice';
export const TagModal = ({
  idTask,
  visible,
  onDismiss,
  type,
  value,
  nameTask,
}: Props) => {
  const [textValue, setTextValue] = useState(value);
  const dispatch = useAppDispatch();

  const onUpsertTag = (textValue: string) => {
    const update = {id: idTask, name: nameTask, nameTag: textValue};
    dispatch(addTag(update));
    Alert.alert('Edit Success');
    onDismiss();
  };

  // Display alert Tag
  const displayDeleteTagAlert = () => {
    Alert.alert(
      'Are You Sure!',
      'This action will delete your tag',
      [
        {
          text: 'Delete',
          onPress: deleTag,
        },
        {
          text: 'No Thank',
          onPress: () => console.log('no thank'),
        },
      ],
      {
        cancelable: true,
      },
    );
  };
  // Delete Tag
  const deleTag = () => {
    const dele = {id: idTask, name: nameTask};
    dispatch(deleteTag(dele));
    Alert.alert('Delete Success');
    onDismiss();
    
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onDismiss();
     
      }}>
        
      <View style={styles.modalBackground}>
        <View style={styles.modalContainer}>
          <Text style={styles.tagTitle}>Add Tags</Text>
          <TextInput
            style={styles.tagGroupAdd}
            placeholder="Add tags"
            value={textValue}
            onChangeText={text=>setTextValue(text)}
          />
          {type === 'add' ? (
            <TouchableOpacity
              onPress={() => onUpsertTag(textValue)}
              style={styles.tagGroupButtonAdd}>
              <Text style={styles.addText}>Add</Text>
            </TouchableOpacity>
          ) : (
            <View style={styles.editTag}>
              <TouchableOpacity onPress={displayDeleteTagAlert}>
                <Text style={styles.removeTag}>Remove</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onUpsertTag(textValue)}>
                <Text style={styles.saveEditTag}>Save</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  tagTitle: {
    width: 200,
    height: 45,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 20,
    lineHeight: 24,
    color: '#303030',
    padding: 10,
    /* Inside auto layout */
  },
  tagGroupAdd: {
    height: 50,
    width: 250,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fff',
    color: '#C5C5C5',
    padding: 10,
    fontSize: 20,
  },
  tagGroupButtonAdd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    gap: 10,
    height: 39,
    /* Inside auto layout */
    order: 2,
    alignSelf: 'stretch',
  },
  addText: {
    width: 32,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
  },
  editTag: {
    /* Auto layout */

    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    gap: 10,
    width: 320,
    height: 39,
    /* Inside auto layout */
  },

  removeTag: {
    width: 63,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#BF1616',
    /* Inside auto layout */
    flexGrow: 0,
  },
  saveEditTag: {
    width: 39,
    height: 19,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    color: '#F7CF00',
    /* Inside auto layout */
    flexGrow: 0,
  },


  
});
