export type ToDoStackNavigator = {
  Home: undefined;
  AddTask: undefined;
  DetailTask: {
    id: number;
    name: string;
    nameTag: string;
  };
};
