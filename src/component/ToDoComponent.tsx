import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import RadioComponent from './RadioComponent';
import {useAppSelector} from '../redux/app/hook';
import {ToDoStackNavigator} from '../navigation/index';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';

type DetailScreenNavigation = NativeStackNavigationProp<
  ToDoStackNavigator,
  'DetailTask'
>;
const ToDoComponent = () => {
  const navigation = useNavigation<DetailScreenNavigation>();
  const listTasks = useAppSelector(state => state.tasks);

  return (
    <View>
      {listTasks.map((name, index) => (
        
        <TouchableOpacity
          key={index}
          style={styles.listItem}
          onPress={() =>
            navigation.navigate('DetailTask', {
              id: index,
              name: name.text,
              nameTag: name.tag,
            })
          }>
      
            
          <RadioComponent
            childRadio={{
              id: index,
              name: name.text,
              nameTag: name.tag,
              step: name.step,
            }}
          />
  
          <View style={styles.showListTag}>
            <Text style={styles.listTasks}>{name.text}</Text>
            <Text>{name.tag} </Text>
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  listItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    gap: 16,
    width: 300,
    height: 50,
    /* Inside auto layout */
    flexGrow: 0,
    marginBottom: 15,
  },
  Ellipse: {
    width: 32,
    height: 32,
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderWidth: 2,
    borderStyle: 'solid',
    borderRadius: 50,
    /* Inside auto layout */

    flexGrow: 0,
  },
  showListTag: {
    height: 60,
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '400',
    lineHeight: 29,
    marginLeft: 10,
    color: '#000000',
    /* Inside auto layout */
    flexGrow: 0,
  },
  listTasks: {
    fontSize: 20,
  },

  tagTask: {
    display: 'flex',
    width: 340,
    height: 19,
    flexWrap: 'wrap',
  },
});
export default ToDoComponent;
