import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import TodolistScreen from '../screens/TodolistScreen';
import AddTaskScreen from '../screens/AddTaskScreen';
import DetailTaskScreen from '../screens/DetailTaskScreen';
import {ToDoStackNavigator} from './index'

const ToDoAp = () => {
  const Stack = createNativeStackNavigator<ToDoStackNavigator>();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={TodolistScreen}
          options={{
            title: '',
            headerShadowVisible: false,
            headerStyle: {
              backgroundColor: '#FFF500',
            },
          }}
        />
        <Stack.Screen
          name="AddTask"
          component={AddTaskScreen}
          options={{
            title: '',
            headerShadowVisible: false,
            headerStyle: {
              backgroundColor: '#303030',
            },
            headerTintColor: '#ffffff',
          }}
        />
        <Stack.Screen
          name="DetailTask"
          component={DetailTaskScreen}
          options={{
            title: '',
            headerShadowVisible: false,
            headerStyle: {
              backgroundColor: '#303030',
            },
            headerTintColor: '#ffffff',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default ToDoAp;
